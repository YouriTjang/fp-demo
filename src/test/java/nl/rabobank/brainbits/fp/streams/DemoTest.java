package nl.rabobank.brainbits.fp.streams;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class DemoTest {
    private List<Account> accounts;

    @BeforeEach
    void setUp() {
        accounts = Arrays.asList(
                new Account("Henk", "123", Arrays.asList(new Balance(BalanceType.CLBD, BigDecimal.ONE)), 0, true),
                new Account("Ingrid", "312", Arrays.asList(new Balance(BalanceType.CLBD, BigDecimal.ZERO), new Balance(BalanceType.XPCD, BigDecimal.TEN)), 0, false),
                new Account("Jan", "213", Arrays.asList(new Balance(BalanceType.CLBD, BigDecimal.TEN)), 1, true)
        );
    }

    @ParameterizedTest
    @ValueSource(classes = {StreamsDemo.class, ForLoopDemo.class, KotlinDemo.class})
    void getActiveAccounts(Class<Demo> demo) throws IllegalAccessException, InstantiationException {
        assertThat(demo.newInstance().getActiveAccounts(accounts)).containsExactly(
                accounts.get(0), accounts.get(2)
        );
    }

    @ParameterizedTest
    @ValueSource(classes = {StreamsDemo.class, ForLoopDemo.class, KotlinDemo.class})
    void getAccountHolders(Class<Demo> demo) throws IllegalAccessException, InstantiationException {
        assertThat(demo.newInstance().getAccountHolders(accounts)).containsExactly(
                "Henk",
                "Ingrid",
                "Jan"
        );
    }

    @ParameterizedTest
    @ValueSource(classes = {StreamsDemo.class, ForLoopDemo.class, KotlinDemo.class})
    void getAccountsByAccountGroup(Class<Demo> demo) throws IllegalAccessException, InstantiationException {
        assertThat(demo.newInstance().getAccountsByAccountGroup(accounts))
                .extracting(0, 1)
                .containsExactly(
                        Arrays.asList(accounts.get(0), accounts.get(1)),
                        Arrays.asList(accounts.get(2))
                );
    }

    @ParameterizedTest
    @ValueSource(classes = {StreamsDemo.class, ForLoopDemo.class, KotlinDemo.class})
    void getTotalBalance(Class<Demo> demo) throws IllegalAccessException, InstantiationException {
        assertThat(demo.newInstance().getTotalBalance(accounts))
                .isEqualTo(BigDecimal.valueOf(11L));
    }

}