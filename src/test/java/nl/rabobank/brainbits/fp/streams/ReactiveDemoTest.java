package nl.rabobank.brainbits.fp.streams;

import io.reactivex.Observable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class ReactiveDemoTest {
    ReactiveDemo demo;
    private List<Account> accounts;

    @BeforeEach
    void setUp() {
        demo = new ReactiveDemo();
        accounts = Arrays.asList(
                new Account("Henk", "123", Arrays.asList(new Balance(BalanceType.CLBD, BigDecimal.ONE)), 0, true),
                new Account("Ingrid", "312", Arrays.asList(new Balance(BalanceType.CLBD, BigDecimal.ZERO), new Balance(BalanceType.XPCD, BigDecimal.TEN)), 0, false),
                new Account("Jan", "213", Arrays.asList(new Balance(BalanceType.CLBD, BigDecimal.TEN)), 1, true)
        );
    }

    @Test
    void getActiveAccounts() {
        final List<Account> result = new ArrayList<>();

        demo.getActiveAccounts(Observable.fromIterable(accounts), result::add);
        assertThat(result)
                .containsExactlyInAnyOrder(accounts.get(0), accounts.get(2));
    }


    @Test
    void getAccountHolders() {
        final List<String> result = new ArrayList<>();
        demo.getAccountHolders(Observable.fromIterable(accounts), result::add);

        assertThat(result).containsExactlyInAnyOrder(
                "Henk",
                "Ingrid",
                "Jan"
        );
    }
}