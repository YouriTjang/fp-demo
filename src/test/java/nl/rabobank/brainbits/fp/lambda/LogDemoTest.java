package nl.rabobank.brainbits.fp.lambda;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class LogDemoTest {
    LogDemo logDemo;

    @BeforeEach
    void setUp() {
        logDemo = new LogDemo();
    }

    @Test
    void log1() {
        logDemo.logAlwaysComputes();
    }

    @Test
    void log2() {
        logDemo.logNotAlwaysComputes();
    }

    @Test
    void log3() {
        logDemo.logNotAlwaysComputesWithLambda();
    }
}