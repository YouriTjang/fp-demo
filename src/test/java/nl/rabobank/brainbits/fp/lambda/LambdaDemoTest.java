package nl.rabobank.brainbits.fp.lambda;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class LambdaDemoTest {

//// JUnit4
//    @Test(expected = NumberFormatException.class)
//    void toIntThrowsException() {
//        toInt("whoopsie");
//    }


    @Test
    void toIntThrowsExceptionTryCatch() {
        try {
            LambdaDemoTest.toInt("whoopsie");
        } catch(NumberFormatException e) {
            assertTrue(true);
        }
    }

    @Test
    void toIntThrowsExceptionJunit5() {
        assertThrows(NumberFormatException.class, () -> LambdaDemoTest.toInt("whoopsie"));
    }

    @Test
    void toIntThrowsExceptionAssertJ() {
        assertThatExceptionOfType(NumberFormatException.class)
                .isThrownBy(() -> LambdaDemoTest.toInt("whoopsie"));
    }

    public static int toInt(String input) {
        return Integer.parseInt(input);
    }
}


















