package nl.rabobank.brainbits.fp.streams;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StreamsDemo implements Demo {

    @Override
    public List<Account> getActiveAccounts(List<Account> accounts) {
        return accounts.stream()
                .filter(account -> account.isActive())
                .collect(Collectors.toList());
    }

    public Stream<Account> getActiveAccounts2(List<Account> accounts) {
        return accounts.stream()
                .filter(account -> account.isActive());
    }



    @Override
    public List<String> getAccountHolders(List<Account> accounts) {
        return accounts.stream()
                .map(account -> account.getAccountHoldeName())
                .collect(Collectors.toList());
    }



    @Override
    public Map<Integer, List<Account>> getAccountsByAccountGroup(List<Account> accounts) {
        return accounts.stream()
                .collect(Collectors.groupingBy(account -> account.getAccountGroup()));
    }




    @Override
    public BigDecimal getTotalBalance(List<Account> accounts) {
        return accounts.stream()
                .flatMap(account -> account.getBalances().stream())
                .filter(balance -> balance.getBalanceType().equals(BalanceType.CLBD))
                .map(balance -> balance.getValue())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public void chainingDemo(List<Account> accounts){
        accounts.stream()
                .filter(account -> account.isActive())
                .map(account -> account.getAccountHoldeName())
                .map(name -> name.charAt(0))
                .collect(Collectors.toList());
    }
}
















