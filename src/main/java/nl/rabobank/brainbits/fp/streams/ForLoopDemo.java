package nl.rabobank.brainbits.fp.streams;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ForLoopDemo implements Demo {

    @Override
    public List<Account> getActiveAccounts(List<Account> accounts) {
        List<Account> filteredAccounts = new ArrayList<>();

        for (Account account : accounts) {
            if (account.isActive()) {
                filteredAccounts.add(account);
            }
        }

        return filteredAccounts;
    }



    @Override
    public List<String> getAccountHolders(List<Account> accounts) {
        ArrayList<String> accountHolders = new ArrayList<>();

        for (Account account : accounts) {
            accountHolders.add(account.getAccountHoldeName());
        }
        return accountHolders;
    }



    @Override
    public Map<Integer, List<Account>> getAccountsByAccountGroup(List<Account> accounts) {
        Map<Integer, List<Account>> map = new HashMap<>();

        for (Account account : accounts) {
            int accountGroup = account.getAccountGroup();
            if (map.get(accountGroup) == null) {
                map.put(accountGroup, new ArrayList<>());
            }
            List<Account> accountListFromGroup = map.get(accountGroup);
            accountListFromGroup.add(account);
        }

        return map;
    }




    @Override
    public BigDecimal getTotalBalance(List<Account> accounts) {
        BigDecimal result = BigDecimal.ZERO;
        for (Account account : accounts) {
            for (Balance balance : account.getBalances()) {
                if (balance.getBalanceType().equals(BalanceType.CLBD)) {
                    result = result.add(balance.getValue());
                }
            }
        }
        return result;
    }

}












