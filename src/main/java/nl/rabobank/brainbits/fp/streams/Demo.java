package nl.rabobank.brainbits.fp.streams;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface Demo {

    List<Account> getActiveAccounts(List<Account> accounts);

    List<String> getAccountHolders(List<Account> accounts);

    Map<Integer, List<Account>> getAccountsByAccountGroup(List<Account> accounts);

    BigDecimal getTotalBalance(List<Account> accounts);
}
