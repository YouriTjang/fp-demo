package nl.rabobank.brainbits.fp.streams

import java.math.BigDecimal

data class Account(
        val accountHoldeName: String,
        val number: String,
        val balances: List<Balance>,
        val accountGroup: Int,
        val isActive: Boolean
)

data class Balance(
        val balanceType: BalanceType,
        val value: BigDecimal
)

enum class BalanceType { XPCD, CLBD, LRLD }



