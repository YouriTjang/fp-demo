package nl.rabobank.brainbits.fp.streams

import java.math.BigDecimal

class KotlinDemo : Demo {
    override fun getActiveAccounts(accounts: MutableList<Account>): List<Account> =
            accounts.filter { it.isActive }

    override fun getAccountsByAccountGroup(accounts: List<Account>): Map<Int, List<Account>> =
            accounts.groupBy { it.accountGroup }

    override fun getAccountHolders(accounts: List<Account>): List<String> =
            accounts.map { it.accountHoldeName }

    override fun getTotalBalance(accounts: List<Account>): BigDecimal =
            accounts.flatMap { it.balances }
                    .filter { it.balanceType.equals(BalanceType.CLBD) }
                    .map { it.value }
                    .reduce { acc, value -> acc.plus(value) }
}