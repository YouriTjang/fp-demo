package nl.rabobank.brainbits.fp.streams;

import io.reactivex.Observable;

import java.util.function.Consumer;

public class ReactiveDemo {

    public void getActiveAccounts(Observable<Account> accounts, Consumer<Account> consumer) {
        accounts
            .filter(account -> account.isActive())
            .subscribe(account -> consumer.accept(account));
    }

    public void getAccountHolders(Observable<Account> accounts, Consumer<String> consumer) {
        accounts
            .map(account -> account.getAccountHoldeName())
            .subscribe(name -> consumer.accept(name));
    }
}
