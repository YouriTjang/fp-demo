package nl.rabobank.brainbits.fp.lambda;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class LogDemo {

    public void logAlwaysComputes() {
        log.debug("Some computationally intensive result: {}", compute());
    }

    public void logNotAlwaysComputes() {
        if (log.isDebugEnabled()) {
            log.debug("Some computationally intensive result: {}", compute());
        }
    }

    public void logNotAlwaysComputesWithLambda() {
        log.debug("Some computationally intensive result: {}", () -> compute());
    }

    private String compute() {
        System.out.println("oh noes, compute() computed");
        return "jeej result";
    }
}
