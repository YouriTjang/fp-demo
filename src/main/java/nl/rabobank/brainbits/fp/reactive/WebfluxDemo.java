package nl.rabobank.brainbits.fp.reactive;

import nl.rabobank.brainbits.fp.streams.Account;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.function.Consumer;

public class WebfluxDemo {

    public void getAccountHolderNames(String klid, Consumer<String> consumer) {
        WebClient.builder()
                .baseUrl("uais.pcf.rabobank.nl")
                .build()
                .get()
                .uri("/useraccounts/" + klid)
                .retrieve()
                .bodyToFlux(Account.class)
                .map(account -> account.getAccountHoldeName())
                .subscribe(name -> consumer.accept(name));
    }
}
